export class Event {
  public id: number;
  public type: string;
  public name: string;
  public image: string;
  public description: string;
  public dateStart: Date;
  public dateEnd: Date;
  public sortableDate?: Date;

  constructor(id: number, type: string, name: string, image: string, description: string, dateStart: Date, dateEnd: Date, sortableDate?: Date) {
   this.id = id;
   this.type = type;
   this.name = name;
   this.image = image;
   this.description = description;
   this.dateStart = dateStart;
   this.dateEnd = dateEnd;
   if (sortableDate) { this.sortableDate = sortableDate }
  }
}
