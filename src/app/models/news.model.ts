export class News {
  public id: number;
  public type: string;
  public name: string;
  public image: string;
  public description: string;
  public datePublished: Date;
  public sortableDate?: Date;

  constructor(id: number, type: string, name: string, image: string, description: string, datePublished: Date, sortableDate?: Date) {
    this.id = id;
    this.type = type;
    this.name = name;
    this.image = image;
    this.description = description;
    this.datePublished = datePublished;
    if (sortableDate) { this.sortableDate = sortableDate }
  }
}
