import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

import { Event } from '../models/event.model';
import { News } from '../models/news.model';

import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';
import { Observer } from 'rxjs/Observer';

const BASE_URL = 'https://node-hnapi.herokuapp.com';

@Injectable()
export class DataStorgeService {

  perPage = 4;

  constructor(private http: Http) { }

  public getNews(limit?: number) {

    // emulate http request response
    return Observable.create((observer: Observer<News[]>) => {
      setTimeout(() => {
        if (limit) {
          observer.next(this.news.slice(0, limit));
        } else {
          observer.next(this.news);
        }
        observer.complete();
      }, 500);
    });
  }

  public getInfiniteNews(page: number = 1) {
    // return this.http.get(`${BASE_URL}/news?page=${page}`);

    // emulate http request
    const data = this.sortData(this.news, 'datePublished', 'desc'),
      from = this.perPage * (page - 1),
      to = (this.perPage * page),
      arrLen = data.length;

    return Observable.create((observer: Observer<News[]>) => {
      setTimeout(() => {
        if ( from > arrLen ) {
          return false;
        } else if ( arrLen < to && from < arrLen ) {
          return observer.next(data.slice(from, arrLen));
        } else {
          observer.next(data.slice(from, to));
        }
        observer.complete();
      }, 1000);
    });
  }

  public getEvents(limit?: number) {

    // emulate http request response
    return Observable.create((observer: Observer<Event[]>) => {
      setTimeout(() => {
        if (limit) {
          observer.next(this.events.slice(0, limit));
        } else {
          observer.next(this.events);
        }
        observer.complete();
      }, 500);
    });
  }

  public getInfiniteEvents(page: number = 1) {

    // emulate http request
    const data = this.sortData(this.events, 'dateStart', 'desc'),
      from = this.perPage * (page - 1),
      to = (this.perPage * page),
      arrLen = data.length;

    return Observable.create((observer: Observer<Event[]>) => {
      setTimeout(() => {
        if ( from > arrLen ) {
          return false;
        } else if ( arrLen < to && from <= arrLen ) {
          return observer.next(data.slice(from, arrLen));
        } else {
          observer.next(data.slice(from, to));
        }
        observer.complete();
      }, 1000);
    });
  }

  public search(query) {
    let data = [];

    this.news.filter(
      (item) => {
        if (item.name.indexOf(query) >= 0 || item.description.indexOf(query) >= 0) {
          item.sortableDate = item.datePublished;
          data.push(item);
        }
      }
    );

    this.events.filter(
      (item) => {
        if (item.name.indexOf(query) >= 0 || item.description.indexOf(query) >= 0) {
          item.sortableDate = item.dateStart;
          data.push(item);
        }
      }
    );

    // emulate http request response
    return Observable.create((observer: Observer<any[]>) => {
      setTimeout(() => {
        observer.next(data);
        observer.complete();
      }, 500);
    });
  }

  sortData(array, field, direction) {
    return array.sort(function(a, b){
      if (a[field] < b[field] && direction === 'desc') {
        return 1;
      } else if (a[field] > b[field] && direction === 'asc') {
        return 1;
      } else {
        return -1;
      }
    });
  }

  // static fake data
  img = 'data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4NCjwhLS0gR2VuZXJhdG9yOiBBZG9iZSBJbGx1c3RyYXRvciAxOS4xLjAsIFNWRyBFeHBvcnQgUGx1Zy1JbiAuIFNWRyBWZXJzaW9uOiA2LjAwIEJ1aWxkIDApICAtLT4NCjxzdmcgdmVyc2lvbj0iMS4xIiBpZD0iTGF5ZXJfMSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgeD0iMHB4IiB5PSIwcHgiDQoJIHZpZXdCb3g9IjAgMCAyNTAgMjUwIiBzdHlsZT0iZW5hYmxlLWJhY2tncm91bmQ6bmV3IDAgMCAyNTAgMjUwOyIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSI+DQo8c3R5bGUgdHlwZT0idGV4dC9jc3MiPg0KCS5zdDB7ZmlsbDojREQwMDMxO30NCgkuc3Qxe2ZpbGw6I0MzMDAyRjt9DQoJLnN0MntmaWxsOiNGRkZGRkY7fQ0KPC9zdHlsZT4NCjxnPg0KCTxwb2x5Z29uIGNsYXNzPSJzdDAiIHBvaW50cz0iMTI1LDMwIDEyNSwzMCAxMjUsMzAgMzEuOSw2My4yIDQ2LjEsMTg2LjMgMTI1LDIzMCAxMjUsMjMwIDEyNSwyMzAgMjAzLjksMTg2LjMgMjE4LjEsNjMuMiAJIi8+DQoJPHBvbHlnb24gY2xhc3M9InN0MSIgcG9pbnRzPSIxMjUsMzAgMTI1LDUyLjIgMTI1LDUyLjEgMTI1LDE1My40IDEyNSwxNTMuNCAxMjUsMjMwIDEyNSwyMzAgMjAzLjksMTg2LjMgMjE4LjEsNjMuMiAxMjUsMzAgCSIvPg0KCTxwYXRoIGNsYXNzPSJzdDIiIGQ9Ik0xMjUsNTIuMUw2Ni44LDE4Mi42aDBoMjEuN2gwbDExLjctMjkuMmg0OS40bDExLjcsMjkuMmgwaDIxLjdoMEwxMjUsNTIuMUwxMjUsNTIuMUwxMjUsNTIuMUwxMjUsNTIuMQ0KCQlMMTI1LDUyLjF6IE0xNDIsMTM1LjRIMTA4bDE3LTQwLjlMMTQyLDEzNS40eiIvPg0KPC9nPg0KPC9zdmc+DQo=';
  desc1 = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc eget metus at elit convallis porta. Nam blandit pretium rutrum. Donec condimentum magna libero, sit amet auctor libero gravida et. Ut vel efficitur turpis, vitae sodales enim. Mauris pharetra justo at imperdiet lacinia. Nam quis nisl quis sapien gravida tincidunt. Etiam congue, arcu sed faucibus congue, risus erat volutpat tellus, et ultrices turpis sapien sit amet erat. Donec tellus dolor, egestas in pharetra quis, tincidunt sit amet massa. Sed porta eros vitae elementum tincidunt. Curabitur fermentum, elit vitae pharetra varius, diam ante varius ipsum, at fermentum ipsum velit malesuada ante.';
  desc2 = 'Cras id mauris sapien. Sed ultrices risus tellus, euismod interdum elit ornare a. Fusce finibus turpis ac ex sodales, a imperdiet elit hendrerit. Aenean ullamcorper, tellus a lacinia placerat, enim leo condimentum erat, vel ultrices nulla ex nec felis. Mauris a dictum justo. Mauris id eros sodales, lobortis nisi vel, bibendum nisi. In viverra eget lorem ut feugiat. Nullam sagittis vestibulum volutpat. Fusce lorem leo, vulputate vel enim ac, imperdiet efficitur tortor. Aenean quam tellus, pretium condimentum odio ac, porttitor aliquet metus. Curabitur finibus augue a nisi aliquet commodo. Praesent sagittis metus ipsum, a molestie ligula viverra sit amet. Vivamus sodales consectetur lectus, at venenatis massa laoreet sed. Sed eget bibendum eros. Mauris vulputate, sem id hendrerit mattis, velit justo sagittis mi, eu ultricies tellus neque eu mi. ';
  desc3 = 'Aliquam nec elementum sem. Pellentesque molestie magna a odio sollicitudin, ut consectetur elit tincidunt. Donec ac accumsan erat, ut eleifend ex. Quisque maximus mi non tellus efficitur tristique. Fusce maximus odio non nisl sagittis, at lobortis lacus pulvinar. Fusce finibus a tellus in pulvinar. Sed erat nibh, euismod sit amet mollis ut, interdum et lorem. Ut tincidunt placerat lobortis. ';

  news: News[] = [
    new News(1, 'news', 'news 1', this.img, this.desc1, new Date('12/1/2017')),
    new News(2, 'news', 'news 2', this.img, this.desc2, new Date('12/10/2017')),
    new News(3, 'news', 'news 3', this.img, this.desc3, new Date('12/20/2017')),
    new News(4, 'news', 'news 4', this.img, this.desc1, new Date('12/25/2017')),
    new News(5, 'news', 'news 5', this.img, this.desc2, new Date('12/12/2017')),
    new News(6, 'news', 'news 6', this.img, this.desc3, new Date('12/10/2017')),
    new News(7, 'news', 'news 7', this.img, this.desc1, new Date('12/2/2017')),
    new News(8, 'news', 'news 8', this.img, this.desc2, new Date('12/3/2017')),
    new News(9, 'news', 'news 9', this.img, this.desc3, new Date('12/4/2017')),
    new News(10, 'news', 'news 10', this.img, this.desc1, new Date('12/5/2017')),
    new News(11, 'news', 'news 11', this.img, this.desc2, new Date('12/6/2017')),
    new News(12, 'news', 'news 12', this.img, this.desc3, new Date('12/7/2017')),
    new News(13, 'news', 'news 13', this.img, this.desc3, new Date('12/7/2017'))
  ];
  events: Event[] = [
    new Event(1, 'event', 'event 1', this.img, this.desc3, new Date('12/4/2017'), new Date('6/30/2017')),
    new Event(2, 'event', 'event 2', this.img, this.desc2, new Date('6/2/2017'), new Date('6/29/2017')),
    new Event(3, 'event', 'event 3', this.img, this.desc1, new Date('6/3/2017'), new Date('6/28/2017')),
    new Event(4, 'event', 'event 4', this.img, this.desc2, new Date('6/4/2017'), new Date('6/27/2017')),
    new Event(6, 'event', 'event 5', this.img, this.desc1, new Date('6/5/2017'), new Date('6/26/2017')),
    new Event(7, 'event', 'event 6', this.img, this.desc2, new Date('6/20/2017'), new Date('6/25/2017')),
    new Event(8, 'event', 'event 7', this.img, this.desc2, new Date('6/14/2017'), new Date('6/24/2017')),
    new Event(9, 'event', 'event 8', this.img, this.desc1, new Date('6/5/2017'), new Date('6/23/2017'))
  ];
}
