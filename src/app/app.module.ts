import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { EventsListComponent } from './pages/events-list/events-list.component';
import { NewsListComponent } from './pages/news-list/news-list.component';

import { DataStorgeService } from './shared/data-storge.service';

// import bootstrap components
import { ModalModule } from 'ngx-bootstrap';

import { HomeComponent } from './pages/home/home.component';
import { SafePipe } from './utilities/safe.pipe';
import { EventItemComponent } from './components/event-item/event-item.component';
import { NewsItemComponent } from './components/news-item/news-item.component';
import { OrderByPipe } from './utilities/order-by.pipe';
import { ModalDetailsComponent } from './components/modal-details/modal-details.component';
import { SearchComponent } from './components/search/search.component';
import { FormsModule } from '@angular/forms';
import { InfiniteScrollerDirective } from './directives/infinite-scroller.directive';


@NgModule({
  declarations: [
    AppComponent,
    EventsListComponent,
    NewsListComponent,
    HomeComponent,
    EventItemComponent,
    NewsItemComponent,
    SafePipe,
    OrderByPipe,
    ModalDetailsComponent,
    SearchComponent,
    InfiniteScrollerDirective,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpModule,
    FormsModule,
    ModalModule.forRoot()
  ],
  providers: [DataStorgeService],
  bootstrap: [AppComponent],
  entryComponents: [ModalDetailsComponent]
})
export class AppModule { }
