import { Component, OnInit, OnDestroy } from '@angular/core';
import { DataStorgeService } from '../../shared/data-storge.service';

import { News } from '../../models/news.model';
import { Event } from '../../models/event.model';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit, OnDestroy {
  news: News[];
  events: Event[];

  newsSubscription: Subscription;
  eventsSubscription: Subscription;

  constructor(private dataStorage: DataStorgeService) { }

  ngOnInit() {
    this.newsSubscription = this.dataStorage.getNews(3)
      .subscribe(
        (data: News[]) => {
          this.news = data;
        }
      );
    this.eventsSubscription = this.dataStorage.getEvents(3)
      .subscribe(
        (data: Event[]) => {
          this.events = data;
        }
      );
  }

  ngOnDestroy() {
    this.newsSubscription.unsubscribe();
    this.eventsSubscription.unsubscribe();
  }

}
