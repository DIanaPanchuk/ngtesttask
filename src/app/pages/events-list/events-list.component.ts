import { Component } from '@angular/core';
import { DataStorgeService } from '../../shared/data-storge.service';
import { Event } from '../../models/event.model';

@Component({
  selector: 'app-events-list',
  templateUrl: './events-list.component.html',
  styleUrls: ['./events-list.component.css']
})
export class EventsListComponent {

  events: Event[] = [];
  currentPage = 1;
  scrollCallback;
  reqInProcess = true;

  constructor(private dataStorage: DataStorgeService) {
    this.scrollCallback = this.getEvents.bind(this);
  }

  getEvents() {
    this.reqInProcess = true;
    return this.dataStorage.getInfiniteEvents(this.currentPage).do(this.processData);
  }

  private processData = (news) => {
    this.currentPage++;
    this.reqInProcess = false;
    if ( news.constructor.name === 'Response' ) {
      this.events = this.events.concat(news.json());
    } else {
      this.events = this.events.concat(news);
    }
  }
}
