import { Component } from '@angular/core';
import { DataStorgeService } from '../../shared/data-storge.service';
import { News } from '../../models/news.model';

@Component({
  selector: 'app-news-list',
  templateUrl: './news-list.component.html',
  styleUrls: ['./news-list.component.css']
})
export class NewsListComponent {
  news: News[] = [];
  currentPage = 1;
  scrollCallback;
  reqInProcess = true;
  constructor(private dataStorage: DataStorgeService) {
    this.scrollCallback = this.getNews.bind(this);
  }

  getNews() {
    this.reqInProcess = true;
    return this.dataStorage.getInfiniteNews(this.currentPage).do(this.processData);
  }

  private processData = (news) => {
    this.currentPage++;
    this.reqInProcess = false;
    if ( news.constructor.name === 'Response' ) {
      this.news = this.news.concat(news.json());
    } else {
      this.news = this.news.concat(news);
    }
  }
}
