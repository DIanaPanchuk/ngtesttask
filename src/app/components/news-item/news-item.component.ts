import { Component, Input } from '@angular/core';
import { News } from '../../models/news.model';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/modal-options.class';
import { ModalDetailsComponent } from '../modal-details/modal-details.component';

@Component({
  selector: 'app-news-item',
  templateUrl: './news-item.component.html',
  styleUrls: ['./news-item.component.css']
})
export class NewsItemComponent {
  bsModalRef: BsModalRef;
  constructor(private modalService: BsModalService) {}

  @Input() news: News;
  @Input() view: string; //full or short preview

  public openModalWithComponent() {
    this.bsModalRef = this.modalService.show(ModalDetailsComponent);
    this.bsModalRef.content.type = 'news';
    this.bsModalRef.content.data = this.news;
  }
}
