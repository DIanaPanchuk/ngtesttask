import { Component } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal/modal-options.class';

@Component({
  selector: 'app-modal-event',
  templateUrl: './modal-details.component.html'
})
export class ModalDetailsComponent {

  public data: any = {};
  public type: string = '';

  constructor(public bsModalRef: BsModalRef) {}

}
