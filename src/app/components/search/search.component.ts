import { Component, OnDestroy } from '@angular/core';
import { DataStorgeService } from '../../shared/data-storge.service';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnDestroy {

  query;
  searchResult = [];
  searchStarted = false;
  searching = false;
  searchAllowed = false;
  subscription: Subscription;

  constructor(private dataStorage: DataStorgeService) {}

  doSearch() {
    this.searchStarted = true;

    this.searchAllowed = ( this.query.length < 2 ) ? false : true;

    if ( this.query.length >= 2 ) {
      this.searching = true;
      this.subscription = this.dataStorage.search(this.query)
       .subscribe(
         (data) => {
           this.searchResult = data;
           this.searching = false;
         }
       );
    }
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
