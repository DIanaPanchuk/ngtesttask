import { Component, Input } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/modal-options.class';
import { ModalDetailsComponent } from '../modal-details/modal-details.component';

@Component({
  selector: 'app-event-item',
  templateUrl: './event-item.component.html',
  styleUrls: ['./event-item.component.css']
})
export class EventItemComponent {
  bsModalRef: BsModalRef;
  constructor(private modalService: BsModalService) {}

  @Input() event: Event;
  @Input() view: string; //full or short preview

  public openModalWithComponent() {
    this.bsModalRef = this.modalService.show(ModalDetailsComponent);
    this.bsModalRef.content.type = 'event';
    this.bsModalRef.content.data = this.event;
  }
}
